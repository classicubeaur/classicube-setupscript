#!/bin/sh
#Script to configure classicube version 1.0.1-1

Game=ClassiCube
game=classicube
DIR="/home/$USER/.local/share/$Game"
SYSDIR=/usr/share/games/$Game
image_path="$SYSDIR/classicube.svg"

sleep 3 
if [ -d "$DIR" ]; then
  ### Take action if $DIR exists ###
  echo "Starting game"
  cd $DIR  
  ./$Game
else
  ###  Control will jump here if $DIR does NOT exists ###
  echo "${DIR} not found. Creating config folder in $DIR and setting up game files"
  yad --width=840 --height=100 --info --title="$name Installation" --window-icon="$image_path" --image="$image_path" --text="Welcome to the $name setup, we will begin setting the game up for you."  --button="OK:1" --button="Cancel:0"
  if [ $? -eq 0 ]; then
  echo "Script exited by user"
  exit 0
fi
(
echo "10" ; sleep 1
echo "# Setting game files" ; sleep 1
echo "20" ; sleep 1
mkdir -p $DIR 
echo "50" ; sleep 1
echo "# Moving game files.." ; sleep 1
cp $SYSDIR/$Game $DIR
echo "100" ; sleep 1
cd $DIR
./$Game
) |
zenity --progress \
  --width=550 \
  --height=100 \
  --title="Setting up ClassiCube" \
  --text="Preparing to setup game..." \
  --percentage=0 \
  --auto-close

if [ "$?" = -1 ] ; then
        zenity --error \
          --text="Update canceled."
fi
fi
